package com.strings;

import com.strings.services.StringsWork;
import com.strings.services.ToStrings;
import com.strings.services.TransFunction;

public class Main {

    public static void main(String[] args) {

        ToStrings toStrings = new ToStrings();

        TransFunction transFunction = new TransFunction();

        StringsWork stringsWork = new StringsWork();

        //1.Вывести в одну строку символы:

        // - английского алфавита от ‘A’ до ‘Z’
        // - английского алфавита от ‘z’ до ‘a’
        // - русского алфавита от ‘а’ до ‘я’
        // - цифры от ‘0’ до ‘9’
        // - печатного диапазона таблицы ASCII

        toStrings.outputString();

        //2. Написать и протестировать функции преобразования:

        // - целого  числа в строку
        System.out.println(transFunction.transIntToString(159));
        // - вещественного числа в строку
        System.out.println(transFunction.transDoubleToString(154.4569));
        // - строки в целое число
        System.out.println(transFunction.transStringToInt("45"));
        // - строки в вещественное число
        System.out.println(transFunction.transStringToDouble("465.745"));

        //3. Написать и протестировать функции работы со строками:

        //Дана строка, состоящая из слов, разделенных пробелами и знаками препинания. Определить длину самого короткого слова.
        System.out.println(stringsWork.getLengthShortWord("Саша любила мороженое, даже когда болела. Она " +
                "знала, что врачи помогут не будет?!"));

        //Дан массив слов. Заменить последние три символа слов, имеющих заданную длину на символ "$"
        String[] wordsArray = {"Дан", "массив", "слов", ".", "Заменить", "последние", "три", "символа", "слов", "на"};
        stringsWork.replaceThreeLastSymbol(20, wordsArray);

        //Добавить в строку пробелы после знаков препинания, если они там отсутствуют.
        stringsWork.addSpaceAfterPunctuationMarks("Саша любила мороженое, даже когда;болела.Она знала, что");

        //Оставить в строке только один экземпляр каждого встречающегося символа.
        stringsWork.deleteRepeatChar("Прикол вот,такой республи665канский");

        //Подсчитать количество слов во введенной пользователем строке.
//        System.out.println(stringsWork.countWords());

        //Удалить из строки ее часть с заданной позиции и заданной длины.
        System.out.println(stringsWork.deletePartString("Прикол вот,такой республи665канский",5,9));

        //Перевернуть строку, т.е. последние символы должны стать первыми, а первые последними.
        System.out.println(stringsWork.reverseArray("Прикол вот,такой республи665канский"));

        //В заданной строке удалить последнее слово.
        System.out.println(stringsWork.deleteLastWord("Сегодня день такой похожий, ни на кого не похожий rehe!"));

    }
}
