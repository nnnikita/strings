package com.strings.services;

public class ToStrings {
    public void outputString () {
        //from 'A' to 'Z';
        for (char i = 65; i < 91; i++) {
            System.out.print(i + " ");
        }
        //from 'z' to 'a'
        System.out.println();
        for (char i = 122; i > 96; i--) {
            System.out.print(i + " ");
        }
        //from 'а' to 'я'
        System.out.println();
        for (char i = 'а'; i <= 'я'; i++) {
            System.out.print(i + " ");
        }
        //from '0' to '9'
        System.out.println();
        for (char i = 48; i < 58; i++) {
            System.out.print(i + " ");
        }
        //print range
        System.out.println();
        for (char i = 32; i < 1112; i++) {
            if ((i > 31 && i < 127) || (i > 1039 && i < 1104)) {
                System.out.print(i);
            }
        }
        System.out.println("ҐґЄєЇїІі");
    }
}
