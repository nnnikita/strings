package com.strings.services;

public class TransFunction {
    //целого  числа в строку
    public String transIntToString (int number) {
        String str = Integer.toString(number);
        return str;
    }
    //вещественного числа в строку
    public String transDoubleToString (double number) {
        String str = String.valueOf(number);
        return str;
    }
    //строки в целое число
    public int transStringToInt (String number) {
        int integer = Integer.parseInt(number);
        return integer;
    }
    //строки в вещественное число
    public double transStringToDouble (String number) {
        double doubler = Double.parseDouble(number);
        return doubler;
    }


}
