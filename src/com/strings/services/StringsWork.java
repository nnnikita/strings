package com.strings.services;

import java.util.Arrays;
import java.util.Scanner;

public class StringsWork {

    Scanner scanner = new Scanner(System.in);

    public int getLengthShortWord(String sentence) {
        String[] mass = sentence.split(" |,|\\.|!|\\?|:|;");
        int lengthShortWord = 100;
        for (int i = 0; i < mass.length; i++) {
            int tempLength = mass[i].length();
            if (tempLength != 0 && tempLength < lengthShortWord) {
                lengthShortWord = tempLength;
            }
        }
        return lengthShortWord;


    }

    public void replaceThreeLastSymbol(int wordLength, String[] wordsArray) {
        String onePart;
        int registr = 0;
        if (wordLength >= 3) {
            for (int i = 0; i < wordsArray.length; i++) {
                int tempLength = wordsArray[i].length();
                if (tempLength == wordLength) {
                    onePart = wordsArray[i].substring(0, wordLength - 3);
                    String finalWord = onePart.concat("$$$");
                    System.out.println(finalWord);
                    registr++;
                }
            }
        } else {
            System.out.println("Неправильно задана длина слов!");
        }
        if (registr == 0) {
            System.out.println("Нет слова с такой длиной");
        }
    }

    public void addSpaceAfterPunctuationMarks(String sentence) {
        sentence = sentence.replaceAll("\\. ", ".");
        sentence = sentence.replaceAll(", ", ",");
        sentence = sentence.replaceAll("; ", ";");
        sentence = sentence.replaceAll(": ", ":");
        sentence = sentence.replaceAll("! ", "!");
        sentence = sentence.replaceAll("\\? ", "?");

        sentence = sentence.replaceAll("\\.", ". ");
        sentence = sentence.replaceAll(",", ", ");
        sentence = sentence.replaceAll(";", "; ");
        sentence = sentence.replaceAll(":", ": ");
        sentence = sentence.replaceAll("!", "! ");
        sentence = sentence.replaceAll("\\?", "? ");

        System.out.println(sentence);
    }

    public void deleteRepeatChar(String sentence) {
        char[] strArr = sentence.toCharArray();
        for (int i = 0; i < strArr.length; i++) {
            for (int j = i + 1; j < strArr.length; j++) {
                if (strArr[i] == strArr[j]) {
                    strArr[j] = ' ';
                }
            }
        }
        sentence = new String(strArr);
//        sentence = sentence.replaceAll(" ", "");
        System.out.println(sentence);
    }

    public int countWords() {
        System.out.println("Введите ваше предлжение: ");
        String sentence = scanner.nextLine();
        int n = 0;
        String[] wordsArr = sentence.split(" |,|\\.|:|;|!|\\?|\\(|\\)");
        for (int i = 0; i < wordsArr.length; i++) {
            if (wordsArr[i].length()>0) {
                n++;
            }
        }
        return n;
    }

    public String deletePartString (String str, int position, int length) {
        if (position < str.length() && position >= 0 && length < str.length() - position && length >= 0) {
//            String tmp = str.substring(position - 1, position - 1 + length);
//            String[] strArr = str.split(tmp);
//            String changed = Arrays.toString(strArr);
//            changed = changed.replaceAll("\\]|\\[|,", "");
            String firstPart = str.substring(0, position);
            String secondPart = str.substring(position + length, str.length());
            String changed = firstPart.concat(secondPart);
            return changed;
        } else {
            return "WRONG INPUT!";
        }
    }

    public String reverseArray (String str) {
        char [] arrayCh = str.toCharArray();
        String reverseArray = "";
        int size = arrayCh.length;
        for (int i = 0; i < size / 2; i++) {
            char temp = arrayCh[i];
            arrayCh[i] = arrayCh[size - 1 - i];
            arrayCh[size - 1 - i] = temp;
        }
        for (char z:arrayCh) {
            reverseArray += z;
        }
//        reverseArray = Arrays.toString(arrayCh);
        return reverseArray;
    }

    public String deleteLastWord (String str) {
        int lengthLastElement;
        String[] wordsArr = str.split(" |,|\\.|:|;|!|\\?|\\(|\\)");
        lengthLastElement = wordsArr[wordsArr.length-1].length();
        String lastWorld = wordsArr[wordsArr.length-1];
        int lastWordIndex = str.lastIndexOf(lastWorld);
        String firstPartStr = str.substring(0,lastWordIndex);
        String secondPartStr = str.substring(lastWordIndex + lengthLastElement,str.length());
        String updateStr = firstPartStr.concat(secondPartStr);
        return updateStr;
    }

}
